<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DestinasiController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\LupaPasswordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('home', function () {
    return view('tamplate');
});

Route::get('dashboard', function () {
    return view('template_admin');
});

Route::controller(DestinasiController::class)->name('desitnasi.')->prefix('destinasi')->group(function () {
    //nama prefix //nama function di controller // name route
    Route::get('/list', 'listDestinasi')->name('list');
    Route::get('/detail/{id}', 'detailDestinasi')->name('detail');
});

Route::controller(LoginController::class)->name('login.')->prefix('login')->group(function () {
    //nama prefix //nama function di controller // name route
    Route::get('/', 'formLogin')->name('login');
    Route::post('/proses-login', 'ProsesLogin')->name('proses-login');
}); 

Route::controller(RegisterController::class)->name('register.')->prefix('register')->group(function () {
    //nama prefix //nama function di controller // name route
    Route::get('/', 'formRegister')->name('register');
    Route::post('/simpan-register', 'simpanRegister')->name('simpan-register');
}); 

Route::controller(AdminController::class)->name('admin.')->middleware('CekLogin:admin')->prefix('admin')->group(function () {
    //nama prefix //nama function di controller // name route
    Route::get('/', 'dashboardAdmin')->name('dashboard');
    Route::get('/tambah-destinasi', 'addDestinasi')->name('add');
    Route::get('/list-destinasi', 'listDestinasi')->name('list');
    Route::post('/simpan-data', 'simpanData')->name('simpan-data');
    Route::get('/form-edit/{id}', 'formEdit')->name('form-edit');
    Route::post('/edit-data/{id}', 'editData')->name('edit-data');
    Route::get('/delete-data/{id}', 'deleteData')->name('delete-data');
}); 

Route::controller(UserController::class)->name('user.')->middleware('CekLogin:user')->prefix('user')->group(function () {
    //nama prefix //nama function di controller // name route
    Route::get('/', 'dashboardUser')->name('dashboard');
    Route::get('/tambah-destinasi', 'addDestinasi')->name('add');
    Route::get('/list-destinasi', 'listDestinasi')->name('list');
    Route::get('/list-destinasi', 'listDestinasi')->name('list');
    Route::post('/simpan-data', 'simpanData')->name('simpan-data');
    Route::get('/form-edit/{id}', 'formEdit')->name('form-edit');
    Route::post('/edit-data/{id}', 'editData')->name('edit-data');
    Route::get('/delete-data/{id}', 'deleteData')->name('delete-data');
}); 

Route::controller(LogoutController::class)->name('logout.')->prefix('logout')->group(function () {
    //nama prefix //nama function di controller // name route
    Route::get('/', 'logout')->name('proses');
});

Route::controller(LupaPasswordController::class)->name('forgot.')->prefix('forgot')->group(function () {
    //nama prefix //nama function di controller // name route
    Route::get('/', 'formLupaPassword')->name('form-forgot');
    Route::post('/proses', 'prosesLupaPassword')->name('proses');
    Route::get('/reset-password/{token}', 'resetPassword')->name('reset-password');
    Route::post('/proses-reset', 'prosesResetPassword')->name('proses-reset');
});