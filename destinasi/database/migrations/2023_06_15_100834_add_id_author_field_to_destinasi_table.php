<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('destinasi', function (Blueprint $table) {
            $table->unsignedBigInteger('id_author')->nullable();
            $table->foreign('id_author')->references('id')->on('pengguna')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('destinasi', function (Blueprint $table) {
            $table->dropForeign('properti_id_author_foreign');
            $table->dropColumn('id_author');
        });
    }
};
