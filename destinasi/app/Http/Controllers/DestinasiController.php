<?php

namespace App\Http\Controllers;

use App\Models\Destinasi;
use App\Models\Pengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DestinasiController extends Controller
{
    public function listDestinasi()
    {
        $dataDestinasi = Destinasi::select('destinasi.id', 'destinasi.nama', 'destinasi.alamat','destinasi.deskripsi' , 'destinasi.gambar', 'destinasi.jumlah_dilihat', 'pengguna.name')
        ->join('pengguna', 'pengguna.id', '=', 'destinasi.id_author')
        ->get();
        return view('destinasi.list_destinasi', compact('dataDestinasi'));
    }

    public function detailDestinasi($id)
    {
        $getDataById = Destinasi::select('destinasi.id', 'destinasi.nama', 'destinasi.alamat','destinasi.deskripsi' , 'destinasi.gambar', 'pengguna.name')
        ->join('pengguna', 'pengguna.id', '=', 'destinasi.id_author')
        ->where('destinasi.id', $id)->first();

        // Perbarui jumlah dilihat pada database
        Destinasi::where('id', $id)->increment('jumlah_dilihat');

        return view('destinasi.deskripsi_destinasi',compact('getDataById'));
    }
}
