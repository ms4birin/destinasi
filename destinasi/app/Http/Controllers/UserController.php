<?php

namespace App\Http\Controllers;

use App\Models\Destinasi;
use App\Models\Pengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function dashboardUser()
    {
        $userId = Auth::id();
        $dataDestinasi = Destinasi::select('destinasi.id', 'destinasi.nama', 'destinasi.alamat','destinasi.deskripsi' , 'pengguna.name')
        ->join('pengguna', 'pengguna.id', '=', 'destinasi.id_author')
        ->where('destinasi.id_author', $userId)->get();

        $jumlahData = $dataDestinasi->count();
        return view('user.dashboard', compact('jumlahData'));
    }

    public function addDestinasi(Type $var = null)
    {
        return view('user.add_destinasi');
    }

    public function simpanData(Request $req)
    {
        try {
            $userId = Auth::id();

            $datas = $req->all();

            $destinasi = new Destinasi;
            $destinasi->nama = $datas['nama'];
            $destinasi->alamat = $datas['alamat'];
            $destinasi->deskripsi = $datas['deskripsi'];

            if ($req->hasFile('gambar')) {
                $namaGambar = time().rand(100,999).".".$datas['gambar']->getClientOriginalExtension();
                $datas['gambar']->move(public_path().'/img', $namaGambar);
                $destinasi->gambar = $namaGambar;
            }

            $destinasi->id_author = $userId;
            $destinasi->save();

            return redirect()->route('user.list')->with('success', __('Berhasil membuat data'));
        } catch (\Throwable $th) {
            return redirect()->route('user.add')->with('error', __($th->getMessage()));
        }
    }

    public function listDestinasi(Type $var = null)
    {
        $userId = Auth::id();
        $dataDestinasi = Destinasi::select('destinasi.id', 'destinasi.nama', 'destinasi.alamat','destinasi.deskripsi' , 'pengguna.name')
        ->join('pengguna', 'pengguna.id', '=', 'destinasi.id_author')
        ->where('destinasi.id_author', $userId)->get();
        return view('user.list_destinasi', compact('dataDestinasi'));
    }

    public function formEdit($id)
    {
        $getDataById = Destinasi::select('destinasi.id', 'destinasi.nama', 'destinasi.alamat','destinasi.deskripsi' , 'pengguna.name')
        ->join('pengguna', 'pengguna.id', '=', 'destinasi.id_author')
        ->where('destinasi.id', $id)->first();
        return view('user.edit_destinasi', compact('getDataById'));
    }

    public function editData(Request $req, $id)
    {
        try {
            $datas = $req->all();
            if ($req->hasFile('gambar')) {
                $namaGambar = time().rand(100,999).".".$datas['gambar']->getClientOriginalExtension();
                $datas['gambar']->move(public_path().'/img', $namaGambar);
                
            }
            Destinasi::where('id', $id)->update(
                [
                    'nama' => $datas['nama'],
                    'alamat' => $datas['alamat'],
                    'deskripsi' => $datas['deskripsi'],
                    'gambar' => $namaGambar
                ]
            );
            return redirect()->route('user.list')->with('succes', __('Berhasil mengedit data'));
        } catch (\Throwable $th) {
            return redirect()->route('user.form-edit')->with('error', __($th->getMessage()));
        }
    }

    public function deleteData($id)
    {
        try {
            Destinasi::where('id', $id)->delete();
            return redirect()->route('user.list')->with('succes', __('Berhasil menghapus data'));
        } catch (\Throwable $th) {
            return redirect()->route('user.list')->with('error', __($th->getMessage()));
        }
    }
}
