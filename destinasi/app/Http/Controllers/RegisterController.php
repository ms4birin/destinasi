<?php

namespace App\Http\Controllers;

use App\Models\Pengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function formRegister(Type $var = null)
    {
        return view('pengguna.form_register');
    }

    public function simpanRegister(Request $req)
    {
        try {
            //membuat proses validasi
            $this->validate($req,[
                'name' => 'required|string|max:255',
                'username' => 'required|max:255',
                'email' => 'required|string|email|unique:pengguna',
                'password' => 'required|string|min:8',
            ]);
            $datas = $req->all();
            $save_data = new Pengguna;
            $save_data->name = $datas['name'];
            $save_data->username = $datas['username'];
            $save_data->email = $datas['email'];
            $save_data->level = 'user';
            $save_data->password = Hash::make($datas['password']);
            $save_data->save();
            return redirect()->route('register.register')->with('success', __('Berhasil Mendaftar'));
        } catch (\Throwable $th) {
            return redirect()->route('register.register')->with('error', __($th->getMessage()));
        }
    }
}
