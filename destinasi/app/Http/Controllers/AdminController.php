<?php

namespace App\Http\Controllers;

use App\Models\Destinasi;
use App\Models\Pengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function dashboardAdmin()
    {
        $dataDestinasi = Destinasi::select('destinasi.id', 'destinasi.nama', 'destinasi.alamat','destinasi.deskripsi' , 'destinasi.gambar', 'pengguna.name')
        ->join('pengguna', 'pengguna.id', '=', 'destinasi.id_author')
        ->get();

        $jumlahData = $dataDestinasi->count();
        return view('admin.dashboard', compact('jumlahData'));
    }

    public function addDestinasi(Type $var = null)
    {
        return view('admin.add_destinasi');
    }

    // public function listDestinasi(Type $var = null)
    // {
    //     return view('admin.list_destinasi');
    // }

    
    public function simpanData(Request $req)
    {
        try {
            $userId = Auth::id();

            $datas = $req->all();

            $destinasi = new Destinasi;
            $destinasi->nama = $datas['nama'];
            $destinasi->alamat = $datas['alamat'];
            $destinasi->deskripsi = $datas['deskripsi'];

            if ($req->hasFile('gambar')) {
                $namaGambar = time().rand(100,999).".".$datas['gambar']->getClientOriginalExtension();
                $datas['gambar']->move(public_path().'/img', $namaGambar);
                $destinasi->gambar = $namaGambar;
            }

            $destinasi->id_author = $userId;
            $destinasi->save();

            return redirect()->route('admin.list')->with('success', __('Berhasil membuat data'));
        } catch (\Throwable $th) {
            return redirect()->route('admin.add')->with('error', __($th->getMessage()));
        }
    }

    public function listDestinasi(Type $var = null)
    {
        $dataDestinasi = Destinasi::select('destinasi.id', 'destinasi.nama', 'destinasi.alamat','destinasi.deskripsi' , 'pengguna.name')
        ->join('pengguna', 'pengguna.id', '=', 'destinasi.id_author')
        ->get();
        return view('admin.list_destinasi', compact('dataDestinasi'));
    }

    public function formEdit($id)
    {
        $getDataById = Destinasi::select('destinasi.id', 'destinasi.nama', 'destinasi.alamat','destinasi.deskripsi' , 'pengguna.name')
        ->join('pengguna', 'pengguna.id', '=', 'destinasi.id_author')
        ->where('destinasi.id', $id)->first();
        return view('admin.edit_destinasi', compact('getDataById'));
    }

    public function editData(Request $req, $id)
    {
        try {
            $datas = $req->all();
            if ($req->hasFile('gambar')) {
                $namaGambar = time().rand(100,999).".".$datas['gambar']->getClientOriginalExtension();
                $datas['gambar']->move(public_path().'/img', $namaGambar);
                
            }
            Destinasi::where('id', $id)->update(
                [
                    'nama' => $datas['nama'],
                    'alamat' => $datas['alamat'],
                    'deskripsi' => $datas['deskripsi'],
                    'gambar' => $namaGambar
                ]
            );
            return redirect()->route('admin.list')->with('succes', __('Berhasil mengedit data'));
        } catch (\Throwable $th) {
            return redirect()->route('admin.form-edit')->with('error', __($th->getMessage()));
        }
    }

    public function deleteData($id)
    {
        try {
            Destinasi::where('id', $id)->delete();
            return redirect()->route('admin.list')->with('succes', __('Berhasil menghapus data'));
        } catch (\Throwable $th) {
            return redirect()->route('admin.list')->with('error', __($th->getMessage()));
        }
    }

    
}
