<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function formLogin()
    {
        return view('pengguna.form_login');
    }

    public function ProsesLogin(Request $req)
    {
        request()->validate(
            [
                'username' => 'required',
                'password' => 'required',
            ]
        );

        $kredensil = $req->only('username', 'password');
        if (Auth::attempt($kredensil)) {
            $user = Auth::user();
            if ($user->level == 'admin') {
                //jika login berhasil dan levelnya admin maka akan redirect ke controller admin dan ditampilkan dashboard admin
                return redirect()->intended('admin');
            } elseif ($user->level == 'user') {
                //jika login berhasil dan levelnya editor maka akan redirect ke controller editor dan ditampilkan dashboard editor
                return redirect()->intended('user');
            }
            return redirect()->intended('/');
        }

        return redirect('login')
            ->withInput()
            ->withErrors(['login_gagal' => 'These credentials do not match our records.']);
    }
}
