<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destinasi extends Model
{
    protected $table = "destinasi";

    public $timestamps = true;

    protected $fillable = [
        'nama',
        'alamat',
        'deskripsi',
        'gambar',
        'id_author'
    ];
}
