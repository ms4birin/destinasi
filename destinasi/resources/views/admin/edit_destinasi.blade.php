@extends('template_admin')

@section('add')
    <!-- add product -->
    <div id="page-wrapper">
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Add Destination
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i> Edit Destination
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->
    
            <div class="row">
                <div class="col-lg-12">
                    @if (session('error'))
                    {{ session('error') }}
                @endif
                @if (session('succes'))
                    {{ session('succes') }}
                @endif
                    <form action="{{ route('admin.edit-data', $getDataById->id) }}" method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Nama</label>
                            <input class="form-control" name="nama" id="nama" value="{{ $getDataById->nama }}">
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <input class="form-control" name="alamat" id="alamat" value="{{ $getDataById->alamat }}">
                        </div>

                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea class="form-control" rows="3" name="deskripsi" id="deskripsi" aria-valuemax="{{ $getDataById->deskripsi }}">{{ $getDataById->deskripsi }}</textarea>
                        </div>   
    
                        <div class="form-group">
                            <label>Gambar</label>
                            <input type="file" name="gambar" id="gambar">
                        </div>
                        <br><br>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn btn-primary">Reset</button>
                    </form>
                </div>
            </div>
        </div><br><br><br><br><br><br>
    </div>
    

@endsection