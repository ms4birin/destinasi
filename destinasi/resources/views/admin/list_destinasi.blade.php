@extends('template_admin')

@section('list')
<div id="page-wrapper">
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Destinations List
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-table"></i> Destinations List
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <a href="{{ route('admin.add') }}"><button class="btn btn-primary"> add destinasi </button></a> 

        

        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <br>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Destinasi</th>
                                <th>Alamat</th>
                                <th>Autor</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @php $no=1; @endphp
                            @foreach ($dataProperti as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $row->nama_kamar }}</td>
                                    <td>{{ $row->nama_properti }}</td>
                                    <td>{{ $row->jumlah_properti }}</td>
                                    <td>
                                        <a href="{{ route('properti.form-edit', $row->id)}}">Edit</a>
                                        <a href="{{ route('properti.delete-data', $row->id)}}" onclick="return hapus();">Delete</a>
                                    </td>
                                </tr>
                            @endforeach --}}
                            @php $no=1; @endphp
                            @foreach ($dataDestinasi as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $row->nama }}</td>
                                    <td>{{ $row->alamat }}</td>
                                    <td>{{ $row->name }}</td>
                                    <td>
                                        <a href="{{ route('admin.form-edit', $row->id)}}"><i class="fa fa-edit"></i>edit</a> | 
                                        <a style="color: rgb(222, 34, 34)" href="{{ route('admin.delete-data', $row->id)}}" onclick="return confirm('Apakah anda yakin ingin menghapus data?');"><i class="fa fa-trash"></i> delete</a></td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div><br><br><br><br><br><br><br><br><br><br>
</div>

@endsection

