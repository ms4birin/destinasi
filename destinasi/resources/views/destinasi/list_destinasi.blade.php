@extends('../tamplate')

@section('list_destination')

    <!-- Destination Start -->
    <div class="container py-5">    
    <div class="row pb-3">
        @foreach ($dataDestinasi as $row)
            <div class="col-md-6 mb-4 pb-2">
                <div class="blog-item">
                    <div class="position-relative">
                        <img class="img-fluid w-100" src="{{ asset('img/'.$row->gambar) }}" alt="" style="width: 200px; height: 400px;">
                    </div>
                    <div class="bg-white p-4">
                        <div class="d-flex mb-2">
                            <p class="text-primary text-uppercase text-decoration-none" >{{ $row->alamat }} | <i class="far fa-eye"></i> {{ $row->jumlah_dilihat }}</p><br>
                            <p class="text-primary text-uppercase text-decoration-none" ></p>
                        </div>
                        <a class="h5 m-0 text-decoration-none" href="{{ route('desitnasi.detail', $row->id)}}">{{ $row->nama }}</a>
                    </div>
                </div>
            </div>
        @endforeach
        

    </div>
  </div>
</div>
  <!-- Destination Start -->
  
@endsection