@extends('../template_login')

@section('register')
<form class="login100-form validate-form" action="{{ route('forgot.proses-reset') }}" method="POST">
    {{ csrf_field() }}
    @if (session('error'))
        {{-- <p style="color: red">{{ session('error') }} </p> --}}
        <span class="alert-inner--text">
                {{ session('error') }}
        </span>
    @endif
    @if (session('success'))
        {{-- <p style="color: green">{{ session('success') }} </p> --}}
        <span class="alert-inner--text">
            {{ session('success') }}
        </span>
    @endif
    <span class="login100-form-title">
        Reset Password
    </span>

    <input type="hidden" name="token" value="{{ $token }}">

    <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
        <input type="text" name="email" class="input100 @error('email') is-invalid @enderror" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="Email">
        <span class="focus-input100"></span>
        <span class="symbol-input100">
            <i class="fa fa-envelope" aria-hidden="true"></i>
        </span>
        @if ($errors->has('email'))
            <span class="error">{{ $errors->first('email') }}</span>
        @endif
    </div>

    <div class="wrap-input100 validate-input" data-validate = "Password is required">
        <input class="input100 @error('password') is-invalid @enderror" type="password" name="password" placeholder="Password" required autocomplete="new-password">
        <span class="focus-input100"></span>
        <span class="symbol-input100">
            <i class="fa fa-lock" aria-hidden="true"></i>
        </span>
        @if ($errors->has('password'))
            <span class="error">{{ $errors->first('password') }}</span>
        @endif
    </div>

    <div class="wrap-input100 validate-input" data-validate = "Password is required">
        <input class="input100 @error('password') is-invalid @enderror" type="password" name="password_confirmation" placeholder="Password" required autocomplete="new-password">
        <span class="focus-input100"></span>
        <span class="symbol-input100">
            <i class="fa fa-lock" aria-hidden="true"></i>
        </span>
        @if ($errors->has('password'))
            <span class="error">{{ $errors->first('password') }}</span>
        @endif
    </div>
    
    <div class="container-login100-form-btn">
        <button class="login100-form-btn">
            Reset
        </button>
    </div>

    

</form>
@endsection