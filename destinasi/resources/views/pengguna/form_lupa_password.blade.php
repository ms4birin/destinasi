@extends('../template_login')

@section('register')
<form class="login100-form validate-form" action="{{ route('forgot.proses') }}" method="POST">
    {{ csrf_field() }}
    <span class="login100-form-title">
        Lupa Password
    </span>

    @if (session('error'))
        <p style="color: red; text-align:center;">{{ session('error') }} </p><br>
    @endif
    @if (session('success'))
        <p style="color: green; text-align:center;">{{ session('success') }} </p><br>
    @endif

    <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
        <input class="input100" type="text" name="email" placeholder="Email">
        <span class="focus-input100"></span>
        <span class="symbol-input100">
            <i class="fa fa-envelope" aria-hidden="true"></i>
        </span>
        @if ($errors->has('email'))
            <span class="error">{{ $errors->first('email') }}</span>
        @endif
    </div>
    
    <div class="container-login100-form-btn">
        <button class="login100-form-btn">
            Kirim
        </button>
    </div>

    <div class="text-center p-t-12">
        <a class="txt2" href="{{ route('login.login') }}">
            Login
            <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
        </a>
    </div>
</form>
@endsection