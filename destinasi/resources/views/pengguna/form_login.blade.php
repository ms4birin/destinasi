@extends('../template_login')

@section('login')
<form action="{{ route('login.proses-login') }}" method="POST" class="login100-form validate-form">
    {{ csrf_field() }}

    @error('login_gagal')
        {{-- <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span> --}}
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{-- <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span> --}}
            <span class="alert-inner--text">
                <strong>Warning!</strong>{{ $message }}
            </span>
        </div>
    @enderror
    <span class="login100-form-title">
        Member Login
    </span>

    <div class="wrap-input100 validate-input" data-validate = "Username is required">
        <input class="input100" type="text" name="username" placeholder="Username">
        <span class="focus-input100"></span>
        <span class="symbol-input100">
            <i class="fa fa-user" aria-hidden="true"></i>
        </span>
        @if ($errors->has('username'))
            <span class="error">{{ $errors->first('username') }}</span>
        @endif
    </div>

    <div class="wrap-input100 validate-input" data-validate = "Password is required">
        <input class="input100" type="password" name="password" placeholder="Password">
        <span class="focus-input100"></span>
        <span class="symbol-input100">
            <i class="fa fa-lock" aria-hidden="true"></i>
        </span>
        @if ($errors->has('password'))
            <span class="error">{{ $errors->first('password') }}</span>
        @endif
    </div>
    
    <div class="container-login100-form-btn">
        <button class="login100-form-btn">
            Login
        </button>
    </div>

    <div class="text-center p-t-12">
        <span class="txt1">
            Forgot
        </span>
        <a class="txt2" href="{{ route('forgot.form-forgot') }}">
            Username / Password?
        </a>
        <br><br><br>
        <a class="txt2" href="{{ route('register.register') }}">
            Create your Account
            <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
        </a>
    </div>
</form>
@endsection