@extends('../template_login')

@section('register')
<form class="login100-form validate-form" action="{{route('register.simpan-register')}}" method="POST">
    {{ csrf_field() }}
    @if (session('error'))
        <p style="color: red">{{ session('error') }} </p>
    @endif
    @if (session('success'))
        <p style="color: green">{{ session('success') }} </p>
    @endif
    <span class="login100-form-title">
        Member Register
    </span>

    <div class="wrap-input100 validate-input" data-validate = "Username is required">
        <input class="input100" type="text" name="name" placeholder="name">
        <span class="focus-input100"></span>
        <span class="symbol-input100">
            <i class="fa fa-user" aria-hidden="true"></i>
        </span>
        @if ($errors->has('name'))
            <span class="error">{{ $errors->first('name') }}</span>
        @endif
    </div>

    <div class="wrap-input100 validate-input" data-validate = "Username is required">
        <input class="input100" type="text" name="username" placeholder="Username">
        <span class="focus-input100"></span>
        <span class="symbol-input100">
            <i class="fa fa-user" aria-hidden="true"></i>
        </span>
        @if ($errors->has('username'))
            <span class="error">{{ $errors->first('username') }}</span>
        @endif
    </div>

    <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
        <input class="input100" type="text" name="email" placeholder="Email">
        <span class="focus-input100"></span>
        <span class="symbol-input100">
            <i class="fa fa-envelope" aria-hidden="true"></i>
        </span>
        @if ($errors->has('email'))
            <span class="error">{{ $errors->first('email') }}</span>
        @endif
    </div>

    <div class="wrap-input100 validate-input" data-validate = "Password is required">
        <input class="input100" type="password" name="password" placeholder="Password">
        <span class="focus-input100"></span>
        <span class="symbol-input100">
            <i class="fa fa-lock" aria-hidden="true"></i>
        </span>
        @if ($errors->has('password'))
            <span class="error">{{ $errors->first('password') }}</span>
        @endif
    </div>
    
    <div class="container-login100-form-btn">
        <button class="login100-form-btn">
            Register
        </button>
    </div>

    <div class="text-center p-t-12">
        <a class="txt2" href="{{ route('login.login') }}">
            Login
            <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
        </a>
    </div>
</form>
@endsection